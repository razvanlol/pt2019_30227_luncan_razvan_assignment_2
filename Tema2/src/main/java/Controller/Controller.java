package Controller;

import Model.SimulationManager;
import View.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;

    public Controller(View view){
        this.view=view;
        this.view.butonStart(new startButton());
    }

    public class startButton implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            int timpServireMin=Integer.parseInt(view.getTpSerMi().getText());
            int timpServireMax=Integer.parseInt(view.getTpSerMa().getText());
            int timpSosireMin=Integer.parseInt(view.getTpSosMi().getText());
            int timpSosireMax=Integer.parseInt(view.getTpSosMa().getText());
            int timpDeSimulare=Integer.parseInt(view.getTpDeSim().getText());
            int numarDeClienti=Integer.parseInt(view.getNrDeClienti().getText());
            int numarDeCozi=Integer.parseInt(view.getNrCozi().getText());
            for(int i=0; i<numarDeCozi; i++){
                JTextField jTextField=new JTextField(20);
                JLabel jLabel=new JLabel("Coada "+i);
                view.getCozi().add(i,jTextField);
                view.getLabelCozi().add(i,jLabel);
                JPanel panel=new JPanel();
                panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));
                panel.add(jTextField);
                panel.add(jLabel);
                view.getLabelQueues().add(panel);
                view.getLabelQueues().revalidate();
                view.getLabelQueues().setVisible(true);
            }
            SimulationManager gen=new SimulationManager(timpSosireMin,timpSosireMax,timpServireMin,timpServireMax,numarDeCozi,timpDeSimulare, numarDeClienti);
            Thread t=new Thread(gen);
            t.start();
        }
    }

}
