package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class View extends JFrame {

    public static ArrayList<JTextField> cozi;
    private ArrayList<JLabel> labelCozi;
    private JTextField tpSosMi;
    private JTextField tpSosMa;
    private JTextField tpSerMi;
    private JTextField tpSerMa;
    private JTextField nrCozi;
    private JTextField nrDeClienti;
    private JTextField tpDeSim;

    private JPanel labeluri;
    private JPanel textfileduri;
    private JPanel panelRight;
    private JPanel mainPanel;
    private JPanel panelLeft;
    private JPanel labelQueues;
    private JPanel queues;

    private JLabel  timpSosireMin;
    private JLabel timpSosireMax;
    private JLabel timpSerivreMin;
    private JLabel timpServireMax;
    private JLabel numarCozi;
    private JLabel timpDeSimulare;
    private JLabel numarDeClienti;

    public static JTextArea afisareStatus;

    private JScrollPane status;

    private JButton butonStart;

    public View(){
        this.cozi=new ArrayList<JTextField>();
        this.labelCozi=new ArrayList<JLabel>();
        this.afisareStatus=new JTextArea();
        this.timpSosireMin = new JLabel();
        this.timpServireMax = new JLabel();
        this.timpSerivreMin = new JLabel();
        this.timpServireMax = new JLabel();
        this.numarCozi=new JLabel();
        this.timpDeSimulare=new JLabel();
        this.mainPanel=new JPanel();
        this.labeluri=new JPanel();
        this.textfileduri=new JPanel();
        this.status=new JScrollPane(afisareStatus);
        this.status.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        this.panelRight=new JPanel();
        this.panelLeft=new JPanel();
        this.queues=new JPanel();
        this.labelQueues=new JPanel();
        this.butonStart=new JButton("Start");

        this.panelRight.setPreferredSize(new Dimension(300,300));
        this.panelLeft.setPreferredSize(new Dimension(300,300));
        //this.queues.setPreferredSize(new Dimension(300,400));
        this.labelQueues.setPreferredSize(new Dimension(300,300));
        this.status.setPreferredSize(new Dimension(250, 300));

        this.numarDeClienti=new JLabel("Numar Clienti ");
        this.timpSosireMin=new JLabel("Timp Sosire Min");
        this.timpSosireMax=new JLabel("Timp Sosire Max");
        this.timpSerivreMin=new JLabel("Timp Servire Min");
        this.timpServireMax=new JLabel("Timp Servire Max");
        this.numarCozi=new JLabel("Numar Cozi");
        this.timpDeSimulare=new JLabel("Timp De Simulare");


        this.tpSosMi=new JTextField("");
        this.tpSosMa=new JTextField("");
        this.tpSerMi=new JTextField("");
        this.tpSerMa=new JTextField("");
        this.nrCozi=new JTextField("");
        this.nrDeClienti=new JTextField("");
        this.tpDeSim=new JTextField("");


        this.panelLeft.setLayout(new BoxLayout(panelLeft,BoxLayout.X_AXIS));
        this.panelRight.setLayout(new BoxLayout(panelRight,BoxLayout.Y_AXIS));
        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.X_AXIS));
        this.labeluri.setLayout(new BoxLayout(labeluri,BoxLayout.Y_AXIS));
        this.textfileduri.setLayout(new BoxLayout(textfileduri,BoxLayout.Y_AXIS));
        this.queues.setLayout(new BoxLayout(queues,BoxLayout.X_AXIS));
        this.labelQueues.setLayout(new BoxLayout(labelQueues,BoxLayout.Y_AXIS));

        this.labeluri.add(timpSosireMin);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(timpSosireMax);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(timpSerivreMin);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(timpServireMax);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(numarCozi);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(numarDeClienti);
        this.labeluri.add(Box.createRigidArea(new Dimension(0,25)));
        this.labeluri.add(timpDeSimulare);

        this.textfileduri.add(tpSosMi);
        this.textfileduri.add(tpSosMa);
        this.textfileduri.add(tpSerMi);
        this.textfileduri.add(tpSerMa);
        this.textfileduri.add(nrCozi);
        this.textfileduri.add(nrDeClienti);
        this.textfileduri.add(tpDeSim);


        this.panelLeft.add(labeluri);
        this.panelLeft.add(textfileduri);

        this.panelRight.add(status);
        this.panelRight.add(butonStart);


        this.mainPanel.add(panelLeft);
        this.mainPanel.add(panelRight);
        this.mainPanel.add(labelQueues);

        this.setTitle("Simulator Cozi");
        this.setSize(new Dimension(1400,500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setContentPane(mainPanel);
        this.pack();
    }

    public void butonStart(ActionListener a1){
        butonStart.addActionListener(a1);
    }

    public JTextField getTpSerMa() {
        return tpSerMa;
    }

    public JTextField getTpSerMi() {
        return tpSerMi;
    }

    public JTextField getTpSosMi() {
        return tpSosMi;
    }

    public JTextField getTpSosMa() {
        return tpSosMa;
    }

    public JTextField getNrDeClienti(){return nrDeClienti;}

    public static JTextArea getAfisareStatus() {
        return afisareStatus;
    }

    public JTextField getTpDeSim() {
        return tpDeSim;
    }

    public JTextField getNrCozi() {
        return nrCozi;
    }

    public static ArrayList<JTextField> getCozi() {
        return cozi;
    }

    public ArrayList<JLabel> getLabelCozi() {
        return labelCozi;
    }

    public JPanel getLabelQueues() {
        return labelQueues;
    }

}