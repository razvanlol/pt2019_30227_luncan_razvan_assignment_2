package Model;

import java.util.ArrayList;

public class Scheduler{

    private ArrayList<Server> servers;
    public Scheduler(){
        this.servers=new ArrayList<Server>();
    }

    public void dispatchTask(Task task){
        int min=Integer.MAX_VALUE;
        Server best=new Server();
        for(Server server:this.servers){
            if(server.getWaitingPeriod().get()<min){
                best=server;
                min=server.getWaitingPeriod().get();
            }

        }
        best.addTask(task);
    }

    public ArrayList<Server> getServers() {
        return servers;
    }
}
