package Model;

public class Task {

    private String numeTask;
    private int timpDeSosire;
    private int timpDeProcesare;

    public Task(String numeTask,int timpDeSosire,int timpDeProcesare){
        this.numeTask=numeTask;
        this.timpDeProcesare=timpDeProcesare;
        this.timpDeSosire=timpDeSosire;
    }

    public int getTimpDeProcesare() {
        return timpDeProcesare;
    }

    public int getTimpDeSosire() {
        return timpDeSosire;
    }

    public String getNumeTask() {
        return numeTask;
    }

    public String toString(){
        return this.numeTask+" "+this.timpDeSosire+" "+this.timpDeProcesare;
    }
}
