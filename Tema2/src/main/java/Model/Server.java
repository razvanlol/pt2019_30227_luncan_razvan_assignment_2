package Model;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


import static View.View.getAfisareStatus;
import static java.lang.Thread.sleep;

public class Server implements Runnable {

    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    public static int ok=0;

    public Server(){
        this.tasks=new ArrayBlockingQueue<Task>(1000);
        this.waitingPeriod=new AtomicInteger(0);
    }

    public void addTask(Task task){
        this.tasks.add(task);
        this.waitingPeriod.set(this.waitingPeriod.get()+task.getTimpDeProcesare());
    }

    public void run() {
        while (ok==0){
            while (!this.tasks.isEmpty()){
                try {
                    Task client = this.tasks.element();
                    sleep(client.getTimpDeProcesare() * 1000);
                    this.waitingPeriod.set(this.waitingPeriod.get() - client.getTimpDeProcesare());
                    String sir="Clientul "+this.tasks.take().getNumeTask()+" a plecat!\n";
                    getAfisareStatus().append(sir);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public String toString(){
        String sir="";
        for(Task tasks : this.tasks){
            sir+=tasks.getNumeTask()+" ";
        }
        return sir;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }
}
