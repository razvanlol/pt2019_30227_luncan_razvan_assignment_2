package Model;

import Model.Scheduler;
import Model.Server;

import java.util.ArrayList;
import java.util.Random;

import static View.View.getAfisareStatus;
import static View.View.getCozi;
import static java.lang.Thread.sleep;

public class SimulationManager implements Runnable {

    private Scheduler scheduler;
    private ArrayList <Task> generatedTasks;
    private ArrayList<Integer> peakTimes;
    private ArrayList<Integer> totalTasksPerServer;
    private int timpSosireMin;
    private int timpSosireMax;
    private int timpServireMin;
    private int timpServireMax;
    private int timpDeSimulare;
    private int numarDeClienti;
    int numarCozi;
    int sum;
    float avg;
    Random random;

    public SimulationManager(int timpSosireMin,int timpSosireMax,int timpServireMin,int timpServireMax,int n,int timpDeSimulare,int numarDeClienti){
        this.totalTasksPerServer=new ArrayList<Integer>();
        this.numarCozi=n;
        this.timpDeSimulare=timpDeSimulare;
        this.timpSosireMin=timpSosireMin;
        this.timpSosireMax=timpSosireMax;
        this.timpServireMin=timpServireMin;
        this.timpServireMax=timpServireMax;
        this.numarDeClienti=numarDeClienti;
        this.random=new Random();
        this.generatedTasks=new ArrayList<Task>();
        this.peakTimes=new ArrayList<Integer>();
        this.scheduler=new Scheduler();
        for(int i=0; i<this.numarCozi; i++){
            this.scheduler.getServers().add(new Server());
            new Thread(this.scheduler.getServers().get(i)).start();
        }
        generateNRandomTasks(timpServireMin,timpServireMax,timpSosireMin,timpSosireMax,numarDeClienti);
    }

    private void generateNRandomTasks(int tsmin,int tsmax,int tsomin,int tsomax,int numarDeClienti){
        for(int i=0; i<numarDeClienti; i++){
            int x = random.nextInt(tsmax+1) + tsmin;
            int y = random.nextInt(tsomax+1) + tsomin;
            Task task=new Task(i+"",y,x);
            this.generatedTasks.add(task);
            sum+=x;
        }
        avg=sum/8;
    }


    int max=Integer.MIN_VALUE;
    int min=Integer.MAX_VALUE;
    int peakTime=0;
    int offPeakTime=0;
    public void run() {
        int currentTime=0;
        while(currentTime<=timpDeSimulare){
            Task aux=null;
            for(Task task:this.generatedTasks){
                if(task.getTimpDeSosire()==currentTime){
                    aux=task;
                    break;
                }
            }
            String sir="Current time "+currentTime+"\n";
            getAfisareStatus().append(sir);
            if(aux!=null){
                String sir1="Vine clientul "+aux.getNumeTask()+" cu timpul de sosire: "+aux.getTimpDeSosire()+" si timp de procesare: "+aux.getTimpDeProcesare()+"\n";
                getAfisareStatus().append(sir1);
                scheduler.dispatchTask(aux);
                generatedTasks.remove(aux);
            }

            int k=0;
            String sir2;
            String sir3;
            int sum=0;
            for(Server server:this.scheduler.getServers()){
                sum+=server.getTasks().size();
                sir2=k+":"+server.toString()+"\n";
                sir3=server.toString()+"\n";
                getCozi().get(k).setText(sir3);
                getAfisareStatus().append(sir2);
                k++;
            }
            if(sum>max){
                this.peakTimes.clear();
                max=sum;
                peakTime=currentTime;
                this.peakTimes.add(currentTime);
            }else if(sum==max){
                this.peakTimes.add(currentTime);
            }
            if(sum<min){
                this.totalTasksPerServer.clear();
                min=sum;
                offPeakTime=currentTime;
                this.totalTasksPerServer.add(currentTime);
            }else if(sum==min){
                this.totalTasksPerServer.add(currentTime);
            }
            currentTime++;
            try{
                sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        Server.ok=1;
        getAfisareStatus().append("Peak time: "+peakTimes+"\n");
        getAfisareStatus().append("Off Peak time: "+totalTasksPerServer+"\n");
        getAfisareStatus().append("Average waiting time: "+avg+"\n");
    }

    /*public static void main(String[] args) {
        View view=new View();
        Controller.Controller controller=new Controller.Controller(view);
    }
    */
}
