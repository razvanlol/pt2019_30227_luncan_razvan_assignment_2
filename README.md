# Queue-Simulator
### A simple queue simulator based on threads.
#### How to use:
##### First steps
* Introduce the limitis in which a client can enter the shop: the lower limit and the upper limit (in seconds).
* Insert the limits on how much time a client can spend while at the counter (in seconds).
* Insert the number of queues.
##### Last step
* Press start.

#### On the middle panel you will be able to see the changes in the shop in real time:
* The queues are shown along with the clients, which are represented by numbers.
* Whenever a new client arrives or leaves, a notification will appear.

#### The panel on the right allows the user to see the cliens and their placement in the queues.